package br.edu.utfpr.cli;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.xml.sax.SAXException;

import br.edu.utfpr.pojo.Arquivo;
import br.edu.utfpr.pojo.Bean;
import br.edu.utfpr.pojo.Biblioteca;
import br.edu.utfpr.pojo.Estante;
import br.edu.utfpr.xml.OperacoesXML;

/**
 * Classe da aplicação responsavel pelo "parse" dos comandos de entrada de dados
 * invocados por meio de terminal, de preferênia o #bash :D </br>
 * 
 * @param String[] args
 * 
 * @author Luan Malaguti
 */
public class Console {
	
	private Biblioteca biblioteca;
	private Estante estante = null;
	
	private CommandLine cmd;
	private CommandLineParser parser;
	private Options opt;
	
	private Console(String[] args){
		this.opt = new Options();
		this.configure();
		this.init(args);
	}
	
	private void configure(){
		
		/**
		 * @param arg: nome da estante
		 * 
		 * Ex: <code> -estante A1 </code>
		 */
		Option estante = OptionBuilder.hasArg().withDescription("Nome da estante").create("estante");
		
		/**
		 * @param arg1: nome do livro
		 * @param arg2: caminho fisico do arquivo
		 * 
		 * Ex: <code> -estante A1 -livro certificado /home/user/certificado.pdf </code>
		 */
		Option livro = OptionBuilder.hasArgs(3).withArgName("nome, path, id").withDescription("Livro da estante").create("livro");
		
		opt.addOption(estante);
		opt.addOption(livro);
		
		try {
			biblioteca = OperacoesXML.lerXml("esperado.xml");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
	
	public void init(String[] args){
		
		try {
			parser = new PosixParser();
			cmd = parser.parse(opt, args);
			
			if(cmd.hasOption("estante")){
				String[] values = cmd.getOptionValues("estante");
				estante = getEstante(values[0]);
				
				if(estante == null){
					estante = criarEstante(values[0]);
				}
			}
			
			if(cmd.hasOption("livro")){
				String[] values = cmd.getOptionValues("livro");
				
				String nome = values[0];
				String path = values[1];
				
				criarLivro(nome, path);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}	
	}
	
	/**
	 * 
	 * @param Lista de elementos que extendem da interface {@link Bean}, e portante implementam o método <code>getId<code>
	 * desde que a lista esteja ordenada
         * @return próximo id
	 */
	public <E extends Bean> int nextId(List<E> lista){
		return lista.get(lista.size() -1).getId() + 1;
	}

	private Estante getEstante(String nome){
		for(Estante e : biblioteca.getEstantes()){
			if(e.getNome().equalsIgnoreCase(nome)){
				return e;
			}
		}
		return null;
	}
	
	private Estante criarEstante(String nome) throws IOException, SAXException, IntrospectionException {
		List<Estante> estantes = biblioteca.getEstantes();
		Collections.sort(estantes);
		
		estante = new Estante(nome, nextId(estantes));
		
		biblioteca.addEstante(estante);
		OperacoesXML.gravarXml(biblioteca, "esperado.xml");
		
		//atualiza a biblioteca
		biblioteca = OperacoesXML.lerXml("esperado.xml");
		return estante;
	}
	
	private void criarLivro(String nome, String path) throws IOException, SAXException, IntrospectionException {
		List<Arquivo> arquivos = new ArrayList<>();
		
		for(Estante e : biblioteca.getEstantes()){
			//if(e.getArquivos() != null)
				arquivos.addAll(e.getArquivos());
		}
		
		Collections.sort(arquivos);
		
		Arquivo file = new Arquivo(nome, false, nextId(arquivos),path);
		estante.addArquivo(file);
		biblioteca.removeEstante(estante);
		biblioteca.addEstante(estante);//FIXME incluir no final do xml, e nao substituir tudo
		OperacoesXML.gravarXml(biblioteca, "esperado.xml");
	}
	
	public static void main(String[] args) {
		new Console(args);
	}
	
}
