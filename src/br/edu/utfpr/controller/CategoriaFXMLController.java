/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.controller;

import br.edu.utfpr.pojo.Bean;
import br.edu.utfpr.pojo.Biblioteca;
import br.edu.utfpr.pojo.Estante;
import br.edu.utfpr.xml.OperacoesXML;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.xml.sax.SAXException;

/**
 * FXML Controller class
 *
 * @author Cassiano
 */
public class CategoriaFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField tx_categoria;
    @FXML
    private Button bt_salvar;
    @FXML
    private Label lb_erro;

    private String nome;

    private Biblioteca biblioteca;
    private Estante estante = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            biblioteca = OperacoesXML.lerXml("esperado.xml");
        } catch (IOException ex) {
            Logger.getLogger(CategoriaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(CategoriaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void salvarClick() throws IOException, SAXException, IntrospectionException {
        System.out.println("Clicou em salvar");
        nome = tx_categoria.getText();
        if (nome.trim().equals("")) {
            lb_erro.setText("Erro! Campo vazio!");
        } else {

            List<Estante> estantes = biblioteca.getEstantes();
            Collections.sort(estantes);

            estante = new Estante(nome, nextId(estantes));

            biblioteca.addEstante(estante);
            OperacoesXML.gravarXml(biblioteca, "esperado.xml");

            //atualiza a biblioteca
            biblioteca = OperacoesXML.lerXml("esperado.xml");

            MainFXMLController.mfc.atualizaDados();
            MainFXMLController.mfc.atualizaCombobox();
        }
        
        tx_categoria.clear();
        

    }

    /**
     *
     * @param Lista de elementos que extendem da interface {@link Bean}, e
     * portante implementam o método <code>getId<code>
     * @return próximo id
     */
    public <E extends Bean> int nextId(List<E> lista) {
        
        if(lista.size() == 0){
            return 0;
        }else{
            return lista.get(lista.size() - 1).getId() + 1;
        }
    }

}
