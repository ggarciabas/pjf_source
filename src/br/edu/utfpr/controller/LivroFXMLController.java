/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.controller;

import br.edu.utfpr.pojo.Arquivo;
import br.edu.utfpr.pojo.Bean;
import br.edu.utfpr.pojo.Biblioteca;
import br.edu.utfpr.pojo.Estante;
import br.edu.utfpr.xml.OperacoesXML;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import org.xml.sax.SAXException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author Cassiano
 */
public class LivroFXMLController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Button bt_salvar;
    @FXML
    private TextField tx_livro;
    @FXML
    private ComboBox<String> cb_categoria;

    private ObservableList livrocb;

    private Biblioteca biblioteca;

    private List<Estante> estantesBiblioteca;

    private OperacoesXML operacoes;

    private int posicaoEstante;

    private Estante estanteAtual;
    @FXML
    private TextField tx_nomeLivro;

    @FXML
    private Label lb_categoria;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            operacoes = new OperacoesXML();
            biblioteca = operacoes.lerXml("esperado.xml");

            estantesBiblioteca = biblioteca.getEstantes();

            // Inserindo categorias no combobox
            livrocb = FXCollections.observableArrayList();
            for (int c = 0; c < estantesBiblioteca.size(); ++c) {
                livrocb.add(estantesBiblioteca.get(c).getNome());
            }

            cb_categoria.setItems(livrocb);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void ClickBuscar() {

        FileChooser fc = new FileChooser();
        FileChooser.ExtensionFilter filtro = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf", "*.chm", "*.mobi", "*.epub");
        fc.getExtensionFilters().add(filtro);

        String userDirectoryString = System.getProperty("user.home");
        File userDirectory = new File(userDirectoryString);
        if (!userDirectory.canRead()) {
            userDirectory = new File("c:/");
        }
        fc.setInitialDirectory(userDirectory);

        File escolhido = fc.showOpenDialog(null);
        String path;
        if (escolhido != null) {
            path = escolhido.getPath();
            tx_livro.setText(path);
        } else {
            path = null;
        }
    }

    @FXML
    public void ClickSalvar() {
        if (cb_categoria.getSelectionModel().getSelectedIndex() != -1) {
            getEstante();

            List<Arquivo> arquivos = new ArrayList<>();

            for (Estante e : biblioteca.getEstantes()) {
                arquivos.addAll(e.getArquivos());
            }

            Collections.sort(arquivos);

            Arquivo file = new Arquivo(tx_nomeLivro.getText(), false, nextId(arquivos), tx_livro.getText());
            estanteAtual.addArquivo(file);
            biblioteca.removeEstante(estanteAtual);
            biblioteca.addEstante(estanteAtual);//FIXME incluir no final do xml, e nao substituir tudo
            try {
                OperacoesXML.gravarXml(biblioteca, "esperado.xml");
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (SAXException e1) {
                e1.printStackTrace();
            } catch (IntrospectionException e1) {
                e1.printStackTrace();
            }

            tx_livro.clear();
            tx_nomeLivro.clear();

            MainFXMLController.mfc.atualizaDados();
            MainFXMLController.mfc.ClickEstante();
        } else {
            lb_categoria.setText("Selecione uma categoria!!");
        }
    }

    /**
     *
     * @param Lista de elementos que extendem da interface {@link Bean}, e
     * portante implementam o método <code>getId<code>
     * @return próximo id
     */
    public <E extends Bean> int nextId(List<E> lista) {
        if (lista.size() == 0) {
            return 0;
        } else {
            return lista.get(lista.size() - 1).getId() + 1;
        }

    }

    public Estante getEstante() {
        posicaoEstante = cb_categoria.getSelectionModel().getSelectedIndex();
        estanteAtual = estantesBiblioteca.get(posicaoEstante);
        for (Estante e : biblioteca.getEstantes()) {
            if (e.getNome().equals(estanteAtual.getNome())) {
                estanteAtual = e;
            }
        }
        return estanteAtual;
    }

}
