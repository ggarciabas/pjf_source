/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
package br.edu.utfpr.controller;

import br.edu.utfpr.pojo.Arquivo;
import br.edu.utfpr.pojo.Biblioteca;
import br.edu.utfpr.pojo.Estante;
import br.edu.utfpr.viewer.RetornoValidacao;
import br.edu.utfpr.viewer.Viewer;
import br.edu.utfpr.xml.OperacoesXML;
import br.edu.utfpr.xml.OperacoesXMLJFX;

import java.awt.TrayIcon.MessageType;
import java.beans.IntrospectionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * @class MainFXMLController
 * @description Classe responsável por gerenciar os recursos do FXML MainFXML.
 *
 * @author Giovanna Garcia Basilio <ggarciabas@gmail.com>
 */
public class MainFXMLController implements Initializable {

    private org.slf4j.Logger logger;
    private OperacoesXMLJFX operacoesjfx;
    private OperacoesXML operacoes;
    private Biblioteca biblioteca;
    private List<Estante> estantesBiblioteca;
    @FXML
    private ScrollPane scroll;
    private ObservableList estanteCb;
    @FXML
    private ComboBox<String> cb_estante;
    private AnchorPane arquivosEstante;
    private VBox vbArquivoSelecionado;
    @FXML
    private Button bt_abrir;
    @FXML
    private Button bt_adicionar;
    @FXML
    private Button bt_remover;
    @FXML
    private Button bt_estante;
    @FXML
    private Button bt_categoria;
    @FXML
    private Button bt_livro;
    private int posicaoArquivo;
    private Estante estanteAtual;
    private int posicaoEstante;
    @FXML
    private Label lb_livro;
    private Viewer visualizador;

    boolean atualizando = false;

    public static MainFXMLController mfc;

    public MainFXMLController() {
        mfc = this;
    }

    @FXML
    public void ClickEstante() {
        vbArquivoSelecionado = null; // nova estante, sem arquivos selecionados

        arquivosEstante.getChildren().clear(); // limpando a estante, atualizando os pdfs...

        if (!atualizando) {
            posicaoEstante = cb_estante.getSelectionModel().getSelectedIndex();
        } else
        {
            atualizando = false;
        }        
        
        System.out.println("posi ... " + posicaoEstante + " estante " + estantesBiblioteca.size());
        estanteAtual = estantesBiblioteca.get(posicaoEstante);

        //System.out.println("Estante: " + posicaoEstante);
        if (posicaoEstante >= 0) {
            // Contruindo as partes da prateleira            
            arquivosEstante.setPrefHeight(0);
            double position = 0;

            int dividido = (int) estanteAtual.getArquivos().size() / 4;
            for (int i = 0, pdf = estanteAtual.getArquivos().size(), pos = 0;
                    i < (dividido + ((dividido < estanteAtual.getArquivos().size()) ? (int) 1 : (int) 0));
                    ++i, pdf -= 4) {
                HBox hBox = new HBox();
                for (int x = 0; x < ((pdf < 4) ? (int) pdf : (int) 4); ++x, ++pos) {
                    VBox vb = new VBox();
                    ImageView imagem = null;

                    // Validando muitos caracteres
                    Label lb = new Label((estanteAtual.getArquivos().get(pos).getNome().length() > 16)
                            ? estanteAtual.getArquivos().get(pos).getNome().substring(0, 13) + "..."
                            : estanteAtual.getArquivos().get(pos).getNome());

                    if (estanteAtual.getArquivos().get(pos).hasImagem()) {
                        // pegar imagem pelo pdf                  
                    } else { // seleciona imagem-padrao
                        InputStream is;
                        try {
                            is = new FileInputStream(new File("res/image/imagem.png"));
                            Image ima = new Image(is);
                            imagem = new ImageView(ima);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                    imagem.setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            ImageView im = (ImageView) event.getSource();
                            VBox vb = (VBox) im.getParent();

                            if (vbArquivoSelecionado != null) {
                                if (vbArquivoSelecionado.equals(vb)) {
                                    return;
                                }
                                //System.out.println("VB: " + vb.getLayoutX());   
                                //System.out.println("modificando");
                                //arquivoSelecionado.getStyleClass().clear();
                                vbArquivoSelecionado.getStyleClass().remove("selecionado");
                                vbArquivoSelecionado.getStyleClass().add("naoselecionado");
                            }
                            vbArquivoSelecionado = vb;
                            HBox hb = (HBox) vb.getParent();
                            if (!vbArquivoSelecionado.getStyleClass().isEmpty()) {
                                vbArquivoSelecionado.getStyleClass().remove("naoselecionado");
                            }
                            vbArquivoSelecionado.getStyleClass().add("selecionado");
                            //System.out.println("Hb: " + hb.getLayoutY());
                            ClickImagem((int) ((vb.getLayoutX() / 128.0) + ((hb.getLayoutY() / 150.0) * 4.0)));
                        }
                    });

                    lb.setLayoutY(120);
                    vb.setAlignment(Pos.CENTER);
                    vb.getChildren().addAll(imagem, lb);
                    hBox.getChildren().add(vb);
                }
                hBox.setLayoutX(0);
                hBox.setLayoutY(position);
                position += 150;
                arquivosEstante.setPrefHeight(arquivosEstante.getPrefHeight() + 150);
                arquivosEstante.getChildren().add(hBox);
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Configurando Logger
        logger = LoggerFactory.getLogger(MainFXMLController.class);

        // Configurando operacoes como Mock
//        operacoesjfx = EasyMock.createMock(OperacoesXMLJFX.class);
//        EasyMock.expect(operacoesjfx.Ler()).andReturn(ConfigurandoBibliotecaMock()).times(1);
//        EasyMock.replay(operacoesjfx);
//        biblioteca = operacoesjfx.Ler();
        atualizaDados();

        // Inserindo categorias no combobox
        estanteCb = FXCollections.observableArrayList();
        for (int c = 0; c < estantesBiblioteca.size(); ++c) {
            //System.out.println("Nome: " + estantesBiblioteca.get(c).getNome());
            estanteCb.add(estantesBiblioteca.get(c).getNome());
        }

        cb_estante.setItems(estanteCb);

        scroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        arquivosEstante = new AnchorPane();
        scroll.setContent(arquivosEstante);

        vbArquivoSelecionado = null;

        lb_livro.setText("");

        visualizador = new Viewer();

    }

    public void atualizaDados() {
        operacoes = new OperacoesXML();
        try {
            biblioteca = operacoes.lerXml("esperado.xml");
        } catch (FileNotFoundException ex) {
            try {
                 File f = new File("esperado.xml");
                 f.createNewFile();
                FileWriter arquivo = new FileWriter(f);  
                arquivo.write("<biblioteca></biblioteca>");  
                arquivo.close(); 
                try {
                    biblioteca = operacoes.lerXml("esperado.xml");
                } catch (IOException ex1) {
                    Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
                } catch (SAXException ex1) {
                    Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } catch (IOException ex1) {
                Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } catch (IOException ex) {
            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

        //System.out.println(biblioteca.getEstantes().get(0).getNome());
        estantesBiblioteca = biblioteca.getEstantes();
    }

    public void atualizaCombobox() {
        atualizando = true;
        // Inserindo categorias no combobox
        estanteCb = FXCollections.observableArrayList();
        for (int c = 0; c < estantesBiblioteca.size(); ++c) {
           // System.out.println("Nome: " + estantesBiblioteca.get(c).getNome());
            estanteCb.add(estantesBiblioteca.get(c).getNome());
        }

        cb_estante.setItems(estanteCb);

//        System.out.println("posicao " + posicaoEstante);     
        
        cb_estante.getSelectionModel().select(posicaoEstante);
    }

//    public Biblioteca ConfigurandoBibliotecaMock() {
//        Biblioteca bib = new Biblioteca();
//        List<Estante> estantes = new ArrayList<>();
//
//        List<Arquivo> arquivos1 = new ArrayList<>();
//        for (int i = 0; i < 20; ++i) {
//            Arquivo ar = new Arquivo();
//            ar.setCaminho("caminho do arquivo");
//            ar.setId(i);
//            ar.setImagem(false);
//            ar.setNome("A: " + i + " E:1");
//            arquivos1.add(ar);
//        }
//        Estante e1 = new Estante();
//        e1.setArquivos(arquivos1);
//        e1.setId(1);
//        e1.setNome("Estante 1");
//
//        List<Arquivo> arquivos2 = new ArrayList<>();
//        for (int i = 0; i < 20; ++i) {
//            Arquivo ar = new Arquivo();
//            ar.setCaminho("caminho do arquivo");
//            ar.setId(i);
//            ar.setImagem(false);
//            ar.setNome("A: " + i + " E:2");
//            arquivos2.add(ar);
//        }
//        Estante e2 = new Estante();
//        e2.setArquivos(arquivos2);
//        e2.setId(2);
//        e2.setNome("Estante 2");
//
//        List<Arquivo> arquivos3 = new ArrayList<>();
//        for (int i = 0; i < 2000; ++i) {
//            Arquivo ar = new Arquivo();
//            ar.setCaminho("caminho do arquivo");
//            ar.setId(i);
//            ar.setImagem(false);
//            ar.setNome("A: " + i + " E:3");
//            arquivos3.add(ar);
//        }
//        Estante e3 = new Estante();
//        e3.setArquivos(arquivos3);
//        e3.setId(3);
//        e3.setNome("Estante 3");
//
//        estantes.add(e1);
//        estantes.add(e2);
//        estantes.add(e3);
//
//        bib.setEstantes(estantes);
//
//        return bib;
//    }
    @FXML
    public void ClickImagem(int pos_arquivo) {
        System.out.println("------- Clickou arquivo: " + pos_arquivo);
        lb_livro.setText((estanteAtual.getArquivos().get(pos_arquivo).getNome().length() > 60)
                ? estanteAtual.getArquivos().get(pos_arquivo).getNome().substring(0, 57) + "..."
                : estanteAtual.getArquivos().get(pos_arquivo).getNome());
        posicaoArquivo = pos_arquivo;
    }

//    @FXML
//    public void ClickAdicionar() {
//        System.out.println("ClickAdicionar");
//    }
    @FXML
    public void ClickRemover() {
        if (vbArquivoSelecionado != null) {
            vbArquivoSelecionado = null;
            System.out.println("Tamaho antes: " + estanteAtual.getArquivos().size());
            Arquivo arq = estanteAtual.getArquivos().get(posicaoArquivo);
            System.out.println("Arquivo: " + arq.getNome());
            estanteAtual.removeArquivo(arq);
            System.out.println("Tamaho depois: " + estanteAtual.getArquivos().size());
            ClickEstante();
            try {
                operacoes.gravarXml(biblioteca, "esperado.xml");
            } catch (IOException ex) {
                Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IntrospectionException ex) {
                Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não foi selecionado qualquer arquivo.");
        }
    }

    @FXML
    public void ClickVisualizar() {
        if (vbArquivoSelecionado != null) {
            RetornoValidacao rv = visualizador.open(estanteAtual.getArquivos().get(posicaoArquivo).getCaminho());

            if (!rv.success) {
                JOptionPane.showMessageDialog(null, rv.getErro());
            }
        } else {
            JOptionPane.showMessageDialog(null, "Não foi selecionado qualquer arquivo.");
        }
    }

    @FXML
    public void ClickCategorias() {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("/br/edu/utfpr/view/CategoriaFXML.fxml"));
            Stage stageCategoria = new Stage();
            stageCategoria.setTitle("Categorias");
            stageCategoria.setScene(new Scene(root, 450, 250));
            stageCategoria.show();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    public void ClickLivro() {
        Parent rootLivro;
        try {
            rootLivro = FXMLLoader.load(getClass().getResource("/br/edu/utfpr/view/LivroFXML.fxml"));
            Stage stageLivro = new Stage();
            stageLivro.setTitle("Livros");
            stageLivro.setScene(new Scene(rootLivro, 550, 250));
            stageLivro.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
