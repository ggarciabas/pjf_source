/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package br.edu.utfpr.pojo;

/**
 * @class Arquivo
 * @description Classe que representa o arquivo.
 *
 * @author Gian Fritsche <gian.fritsche@gmail.com>
 * @author Giovanna Garcia Basilio <ggarciabas@gmail.com>
 */
public class Arquivo implements Bean, Comparable<Arquivo>{
    private String nome;
    private boolean imagem;
    private int id;
    private String caminho;

    public Arquivo() {
    }

    public Arquivo(String nome, boolean imagem, int id, String caminho) {
        this.nome = nome;
        this.imagem = imagem;
        this.id = id;
        this.caminho = caminho;
    }
    
    public Arquivo(String nome, boolean imagem, String caminho) {
        this.nome = nome;
        this.imagem = imagem;
        this.caminho = caminho;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean hasImagem() {
        return imagem;
    }
    
    public boolean isImagem() { //adaptado para padrão pojo
        return imagem;
    }

    public void setImagem(boolean imagem) {
        this.imagem = imagem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    @Override
	public int compareTo(Arquivo o) {
		if (this.id < o.id) {
            return -1;
        }
        if (this.id > o.id) {
            return 1;
        }
        return 0;
	}        
}
