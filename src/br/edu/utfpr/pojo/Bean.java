package br.edu.utfpr.pojo;
/**
 * Interface a ser implementada por todos os objetos que necessitam de auto incremento no id
 * 
 * @author Luan Malaguti
 */
public interface Bean {
	public int getId();
}
