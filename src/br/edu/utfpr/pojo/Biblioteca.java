/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package br.edu.utfpr.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * @class Biblioteca
 * @description Classe que representa a biblioteca.
 *
 * @author Gian Fritsche <gian.fritsche@gmail.com>
 * @author Giovanna Garcia Basilio <ggarciabas@gmail.com>
 */
public class Biblioteca {
    private List<Estante> estantes;

    public Biblioteca() {
        estantes = new ArrayList<>();
    }

    public List<Estante> getEstantes() {
        return estantes;
    }

    public void setEstantes(List<Estante> estantes) {
        this.estantes = estantes;
    }
    
    public void addEstante (Estante e) {
        estantes.add(e);
    }

    public void removeEstante (Estante e) {
        estantes.remove(e);
    }
    
    public void removeAllEstantes () {
        estantes.clear();
    } 
}
