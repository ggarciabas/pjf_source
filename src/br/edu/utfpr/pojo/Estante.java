/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package br.edu.utfpr.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * @class Estante
 * @description Classe que representa a estante.
 *
 * @author Gian Fritsche <gian.fritsche@gmail.com>
 * @author Giovanna Garcia Basilio <ggarciabas@gmail.com>
 */
public class Estante implements Bean, Comparable<Estante>{
   private List<Arquivo> arquivos;
   private String nome;
   private int id;

   public Estante() {
       arquivos = new ArrayList<>();
   }
   
   public Estante(String nome, int id) { 
	   arquivos = new ArrayList<>();
	   this.nome = nome;
       this.id = id;
   }
   
   public Estante(List<Arquivo> arquivos, String nome, int id) { // Adicionado construtor com parametros
       this.arquivos = arquivos;
       this.nome = nome;
       this.id = id;
   }
   
    public List<Arquivo> getArquivos() {
        return arquivos;
    }
    
    public void addArquivo (Arquivo a) {
        arquivos.add(a);
    }

    public void removeArquivo (Arquivo a) {
        arquivos.remove(a);
    }
    
    public void removeAllArquivos () {
        arquivos.clear();
    } 
    
    public void setArquivos(List<Arquivo> arquivos) {
        this.arquivos = arquivos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	@Override
	public int compareTo(Estante o) {
		if (this.id < o.id) {
            return -1;
        }
        if (this.id > o.id) {
            return 1;
        }
        return 0;
	}
      
}
