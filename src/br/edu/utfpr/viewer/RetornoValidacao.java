package br.edu.utfpr.viewer;
/**
 * Classe utilitaria para a validação dos retornos de ações do visualizador de arquivos
 * 
 * @author <a href="luanmalaguti@gmail.com">Luan Malaguti</a>
 */
public class RetornoValidacao {
	public boolean success = true;
	public String erro = null;
	
	public RetornoValidacao() {}
	
	public RetornoValidacao(String erro) {
		this.erro = erro;
		this.success = false;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}
}
