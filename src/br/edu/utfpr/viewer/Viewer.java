package br.edu.utfpr.viewer;

import javax.swing.JOptionPane;

import br.edu.utfpr.viewer.impl.JPedalViewer;
import br.edu.utfpr.viewer.impl.SimpleViewer;

/**
 * Classe que fornece a chamada para visualização dos arquivos da estante
 * 
 * <p> {@link Viewer#open(path)} </p>
 *  
 * @author <a href="luanmalaguti@gmail.com">Luan Malaguti</a>
 */
public class Viewer {
	
	private static ViewerStrategy visualizador;
	
	/**
	 * Chamar este metodo para a abertura de quaisquer arquivos 
	 * 
	 * @param path do arquivo
	 * @return {@link RetornoValidacao}
	 */
	public static RetornoValidacao open(String path){
		String ext = path.substring(path.lastIndexOf('.'));
		
		// Para arquivos que não são do tipo PDF usa o visualizador do SO
		if(!ext.equalsIgnoreCase(".pdf")){
			visualizador = new SimpleViewer();	
		}else{// Para arquivos do tipo PDF, pede ao usuário se quer utilizar o JPedal para visualizar
			int resp = showDialog();
			visualizador = resp == JOptionPane.YES_OPTION ? new JPedalViewer() : new SimpleViewer();  
		}
		
		return visualizador.openFile(path);
	}
	
	/**
	 * Apresenta uma caixa de dialogo interrogativo ao usuário
	 * 
	 * @return 0 para sim | 1 para não
	 */
	private static int showDialog(){
		return JOptionPane.showConfirmDialog(null, "Visualizar com JPedal?", "Arquivo PDF",  JOptionPane.YES_NO_OPTION);
	}
}
