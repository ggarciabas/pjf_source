package br.edu.utfpr.viewer;
/**
 * Define o metodo de abertura dos arquivos para que cada tipo de vizualidor 
 * implemente de acordo com o tipo de abertura (interna ou externa). 
 * 
 * @author <a href="luanmalaguti@gmail.com">Luan Malaguti</a>
 */
public interface ViewerStrategy {
	
	/**
	 * @param Caminho do arquivo
	 * @return {@link RetornoValidacao}
	 */
	public RetornoValidacao openFile(String path);
	
}
