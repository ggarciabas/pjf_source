package br.edu.utfpr.viewer.impl;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;

import org.jpedal.examples.viewer.Commands;
import org.jpedal.examples.viewer.Viewer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.edu.utfpr.viewer.RetornoValidacao;
import br.edu.utfpr.viewer.ViewerStrategy;

/**
 * Implementação que chama o visualizador JPedal para arquivos PDF
 * 
 * @see Viewer
 * 
 * @author <a href="luanmalaguti@gmail.com">Luan Malaguti</a>
 */
public class JPedalViewer implements ViewerStrategy {
	
	private static final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	private JFrame frame;
	private JInternalFrame rootContainer;
	private Viewer viewer;
	
	private static final Logger log = LoggerFactory.getLogger(JPedalViewer.class);
	
	@Override
	public RetornoValidacao openFile(String path) {
		try {
			log.info("Configurando JPedal...");
			
			frame = new JFrame();
			frame.getContentPane().setLayout(new BorderLayout());

			rootContainer = new JInternalFrame(new File(path).getName());

			viewer = new Viewer(rootContainer, null);
			viewer.setupViewer();

			// Adiciona o viewer no container
			frame.add(rootContainer, BorderLayout.CENTER);
			rootContainer.setVisible(true);

			frame.setTitle("Viewer in External Viewer");
			// Configura o tamanho do visualizador com a resolução da tela
			frame.setSize((int) screen.getWidth(),(int) screen.getHeight());
			frame.setVisible(true);
			
			log.info("Arquivo enviado ao JPedal para visualização");
			// Manda vizualizar o arquivo
			viewer.executeCommand(Commands.OPENFILE, new Object[] {path});
			log.info("Arquivo visualizado com sucesso");
			return new RetornoValidacao();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return new RetornoValidacao(e.getMessage());
		}	
	}
}
