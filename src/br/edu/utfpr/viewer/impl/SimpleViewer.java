package br.edu.utfpr.viewer.impl;

import java.awt.Desktop;
import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.edu.utfpr.viewer.RetornoValidacao;
import br.edu.utfpr.viewer.ViewerStrategy;

/**
 * Implementação que chama o visualizador padrão do SO para o arquivo referente.
 * 
 * @author <a href="luanmalaguti@gmail.com">Luan Malaguti</a>
 */
public class SimpleViewer implements ViewerStrategy{
	
	private static final Logger log = LoggerFactory.getLogger(SimpleViewer.class);
	
	/**
	 * @see Desktop#getDesktop()
	 */
	@Override
	public RetornoValidacao openFile(String path) {
		try {
			log.info("Configurando ferramentas para abrir o arquivo...");
			Desktop.getDesktop().open(new File(path));
			log.info("Arquivo visualizado com sucesso");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return new RetornoValidacao(e.getMessage());
		}	
		return new RetornoValidacao();
	}
}
