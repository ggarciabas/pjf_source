package br.edu.utfpr.xml;

import java.beans.IntrospectionException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.betwixt.io.BeanWriter;
import org.apache.commons.betwixt.strategy.DecapitalizeNameMapper;
import org.apache.commons.digester3.Digester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import br.edu.utfpr.pojo.Arquivo;
import br.edu.utfpr.pojo.Biblioteca;
import br.edu.utfpr.pojo.Estante;


/**
 * 
 * @author Gian M. Fritsche 
 * @email gian.fritsche@gmail.com
 * @class OperacoesXML
 * @description classe responsavel pelas ações sobre o arquivo xml que servirá como repositório da biblioteca
 *
 */
public class OperacoesXML {

	public static Biblioteca lerXml(String file) throws IOException, SAXException {
		Logger logger = LoggerFactory.getLogger(OperacoesXML.class);
		logger.info("Efetuando leitura do arquivo "+file);
		
		Digester digester = new Digester();
		digester.setValidating(false);
		digester.addObjectCreate("biblioteca", Biblioteca.class);

		digester.addObjectCreate("biblioteca/estantes/estante", Estante.class);
		digester.addSetNext("biblioteca/estantes/estante", "addEstante",
				"br.edu.utfpr.pojo.Estante");
		digester.addBeanPropertySetter("biblioteca/estantes/estante/id",
				"id");
		digester.addBeanPropertySetter("biblioteca/estantes/estante/nome",
				"nome");

		digester.addObjectCreate("biblioteca/estantes/estante/arquivos/arquivo", Arquivo.class);
		digester.addSetNext("biblioteca/estantes/estante/arquivos/arquivo", "addArquivo",
				"br.edu.utfpr.pojo.Arquivo");
		digester.addBeanPropertySetter("biblioteca/estantes/estante/arquivos/arquivo/id",
				"id");
		digester.addBeanPropertySetter("biblioteca/estantes/estante/arquivos/arquivo/nome",
				"nome");
		digester.addBeanPropertySetter("biblioteca/estantes/estante/arquivos/arquivo/caminho",
				"caminho");
		digester.addBeanPropertySetter("biblioteca/estantes/estante/arquivos/arquivo/imagem",
				"imagem");

		try {
			return (Biblioteca) digester.parse(new File(file));
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw e;
		} catch (SAXException e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static void gravarXml(Biblioteca biblioteca, String file) throws IOException, SAXException, IntrospectionException {
		Logger logger = LoggerFactory.getLogger(OperacoesXML.class);
		logger.info("Efetuando escrita no arquivo "+file);
		BeanWriter writer;
		try {
			writer = new BeanWriter(new FileOutputStream(new File(
					file)));
		} catch (FileNotFoundException e) {
			logger.error(e.getMessage());
			throw e;
		}
		writer.enablePrettyPrint();
		writer.getXMLIntrospector().getConfiguration()
				.setElementNameMapper(new DecapitalizeNameMapper());
		writer.getXMLIntrospector().getConfiguration()
				.setAttributesForPrimitives(false);
		writer.getBindingConfiguration().setMapIDs(false);
		try {
			writer.write(biblioteca);
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw e;
		} catch (SAXException e) {
			logger.error(e.getMessage());
			throw e;
		} catch (IntrospectionException e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
}
