/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package br.edu.utfpr.app;

import javafx.application.Application;
import javafx.stage.Stage;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @class MainTest
 * @description Classe responsável por iniciar a aplicação utilizando JavaFX.
 * (Teste)
 *
 * @author Giovanna Garcia Basilio <ggarciabas@gmail.com>
 */
public class MainTest extends Application {

    private static Stage stage;
    private static Main main;
    private static String[] args;
//    private static OperacoesXMLJFX operacoes;

    public MainTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        main = new Main();    
//        operacoes = EasyMock.createMock(OperacoesXMLJFX.class);
    }

    @Test
    public void testStart() throws Exception {
        main.main(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
    }

}
