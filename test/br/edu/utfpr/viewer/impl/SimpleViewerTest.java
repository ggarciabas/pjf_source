/**
 * 
 */
package br.edu.utfpr.viewer.impl;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.edu.utfpr.viewer.RetornoValidacao;
import br.edu.utfpr.viewer.ViewerStrategy;

/**
 * Testes unitarios da classe {@link SimpleViewer}
 * 
 * @author <a href="luanmalaguti@gmail.com">Luan Malaguti</a>
 */
public class SimpleViewerTest {

	private static final Logger log = LoggerFactory.getLogger(SimpleViewerTest.class);

	private ViewerStrategy viewer;
	private RetornoValidacao rv;

	@Before
	public void setUp() throws Exception {
		log.info("Configurando unidades de teste para " + getClass());
		viewer = new SimpleViewer();
	}

	@Test
	public void openFile() {
		rv = viewer.openFile(new File("./res/pdf/article.pdf").getAbsolutePath());

		if (!rv.isSuccess()) {
			Assert.assertNotNull(rv.getErro());
			log.error(rv.getErro());
		}

		Assert.assertTrue(rv.isSuccess());
	}

}
