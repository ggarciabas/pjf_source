package br.edu.utfpr.xml;

import java.beans.IntrospectionException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import br.edu.utfpr.pojo.Arquivo;
import br.edu.utfpr.pojo.Biblioteca;
import br.edu.utfpr.pojo.Estante;

public class OperacoesXMLTest {

    private static String conteudoEsperado;
    private static Biblioteca b;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        Arquivo a1 = new Arquivo("NS3 Manual", false, 1, "D:\\Utfpr\\6 periodo\\DBF\\Final\\PJF_Ebook\\meusRecursos\\ns-3-manual.pdf");
        Arquivo a2 = new Arquivo("NS3 Tutorial", false, 2, "D:\\Utfpr\\6 periodo\\DBF\\Final\\PJF_Ebook\\meusRecursos\\ns-3-tutorial.pdf");
        Arquivo a3 = new Arquivo("NS3 Model Library", false, 3, "D:\\Utfpr\\6 periodo\\DBF\\Final\\PJF_Ebook\\meusRecursos\\ns-3-model-library.pdf");

        List<Arquivo> c1a = new ArrayList<>();

        c1a.add(a1);
        c1a.add(a2);
        c1a.add(a3);

        Estante c1 = new Estante(c1a, "c1", 1);

        List<Estante> e = new ArrayList<>();
        e.add(c1);

        b = new Biblioteca();
        b.setEstantes(e);

        conteudoEsperado = "  <biblioteca>    <estantes>      <estante>        <arquivos>          <arquivo>            <caminho>/home/gian/Documentos/a1.pdf</caminho>            <id>1</id>            <imagem>false</imagem>            <nome>a1</nome>          </arquivo>          <arquivo>            <caminho>/home/gian/Documentos/a2.pdf</caminho>            <id>2</id>            <imagem>false</imagem>            <nome>a2</nome>          </arquivo>          <arquivo>            <caminho>/home/gian/Documentos/a3.pdf</caminho>            <id>3</id>            <imagem>false</imagem>            <nome>a3</nome>          </arquivo>          <arquivo>            <caminho>/home/gian/Documentos/a4.pdf</caminho>            <id>4</id>            <imagem>false</imagem>            <nome>a4</nome>          </arquivo>        </arquivos>        <id>1</id>        <nome>c1</nome>      </estante>      <estante>        <arquivos/>        <id>2</id>        <nome>c2</nome>      </estante>    </estantes>  </biblioteca>";
    }

    @Test(expected = IOException.class)
    public void testExceptionLerXml() throws IOException, SAXException {
        OperacoesXML.lerXml("naoexiste.xml");
    }

    @Test
    public void lerXml() {
        boolean sucesso = true;
        try {
            OperacoesXML.lerXml("esperado.xml");
        } catch (IOException e) {
            LoggerFactory.getLogger(OperacoesXMLTest.class).error(
                    e.getMessage());
            sucesso = false;
        } catch (SAXException e) {
            LoggerFactory.getLogger(OperacoesXMLTest.class).error(
                    e.getMessage());
            sucesso = false;
        }
        Assert.assertTrue(sucesso);
    }

    @Test
    public void gravarXml() {

        try {
            OperacoesXML.gravarXml(b, "esperado.xml");
        } catch (IOException | SAXException | IntrospectionException e) {
            LoggerFactory.getLogger(OperacoesXMLTest.class).error(
                    e.getMessage());
        }

        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("teste.xml"));
            String conteudo = "";
            while (br.ready()) {
                conteudo += br.readLine();
            }
            br.close();
            Assert.assertEquals(conteudoEsperado, conteudo);
        } catch (FileNotFoundException e) {
            LoggerFactory.getLogger(OperacoesXMLTest.class).error(
                    e.getMessage());
        } catch (IOException e) {
            LoggerFactory.getLogger(OperacoesXMLTest.class).error(
                    e.getMessage());
        }
    }

}
